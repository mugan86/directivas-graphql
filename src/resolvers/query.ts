import { IResolvers } from "graphql-tools";

const query: IResolvers = {
    Query: {
        users() {
            return [
                {
                    id: 1,
                    name: 'Anartz Mugika Ledo',
                    email: 'anartz@udemy.com',
                    password: "1234"
                },
                {
                    id: 2,
                    name: 'Mikel Martinez',
                    email: 'mikel@martinez.com',
                    password: "1234"
                }
            ]
        }
    }
}

export default query;