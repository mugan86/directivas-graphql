import { defaultFieldResolver } from "graphql";
import { SchemaDirectiveVisitor } from "graphql-tools";

class PasswordDirective extends SchemaDirectiveVisitor {
    visitFieldDefinition(field: any) {
        const { resolve = defaultFieldResolver } = field;
        const { min, max } = this.args;
        field.resolve = async function (...args: any) {
            const result = await resolve.apply(this, args);
            if (typeof result === "string") {
                const characters = Math.floor((Math.random() * max) + min);
                return '*'.repeat(characters);
            }
            return result;
        };
    }
}

export default PasswordDirective;
