import { GraphQLSchema } from "graphql";
const { upperFirst } = require('apollo-directives')
import 'graphql-import-node';
import typeDefs from './schema.graphql';
import resolvers from './../resolvers/resolversMap';
import { makeExecutableSchema } from "graphql-tools";
import PasswordDirective from "../directives/password";

const schema: GraphQLSchema = makeExecutableSchema({
    typeDefs,
    resolvers,
    schemaDirectives: {
        upperFirst,
        password: PasswordDirective
    }
});

export default schema;