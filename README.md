# Directivas en GraphQL

Aprenderemos el uso de directivas dentro de los schemas, usando las existentes y creando las nuestras propias.

## Enlace al artículo:
[Directivas Schemas GraphQL](https://medium.com/@mugan86/tips-graphql-uso-de-directivas-de-schema-877e5e09ee4a?sk=e5d21370e35b8da93dd40b6f236a333d)